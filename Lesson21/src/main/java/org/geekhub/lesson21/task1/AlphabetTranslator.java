package org.geekhub.lesson21.task1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

class AlphabetTranslator {

    private static final Logger log = Logger.getLogger(AlphabetTranslator.class.getName());

    String translateRegularString(String regularString) {
        InputStream inputStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("alphabet.properties");
        Properties alphabetProperties = new Properties();
        try {
            if (inputStream != null) {
                alphabetProperties.load(inputStream);
            } else {
                throw new FileNotFoundException("Resource not found!");
            }
        } catch (IOException e) {
            log.info(e.getMessage() + "");
            throw new RuntimeException(e.getMessage());
        }
        return convertStringToMorse(alphabetProperties, regularString);

    }


    private String convertStringToMorse(Properties propertyFile, String input) {
        String finalString = "";
        String[] inputLetters = input.split("");
        for (String letter : inputLetters) {
            finalString = finalString.concat(getMorseLetter(propertyFile, letter));
        }
        return finalString;
    }


    private String getMorseLetter(Properties propertyFile, String letter) {
        if (letter.isBlank()) {
            return "/";
        } else if (propertyFile.getProperty(letter.toUpperCase()) == null) {
            throw new IllegalArgumentException("Incorrect input!");
        }
        return propertyFile.getProperty(letter.toUpperCase());
    }
}
