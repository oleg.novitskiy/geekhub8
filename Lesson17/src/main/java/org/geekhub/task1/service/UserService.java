package org.geekhub.task1.service;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAllUsers();

    Optional<User>find(String login);

    void add(User user);

    void delete(User user);

    void update(User user);
}