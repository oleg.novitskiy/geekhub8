package org.geekhub.task1;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {
    @Autowired
    private Environment env;

    public ConnectionPool() {

    }

    Connection getConnection() {

        PoolProperties properties = new PoolProperties();
        properties.setUrl(env.getProperty("url"));
        properties.setDriverClassName(env.getProperty("driverClassName"));
        properties.setUsername(env.getProperty("login"));
        properties.setPassword(env.getProperty("password"));

        DataSource datasource = new DataSource();
        datasource.setPoolProperties(properties);
        Connection connection;
        try {
            connection = datasource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return connection;
    }
}
