package org.geekhub.lesson11.task2;

import org.geekhub.lesson11.ObjectsEquals;
import org.geekhub.lesson11.task1.CanNotCloneException;
import org.geekhub.lesson11.task1.CloneCreatorImpl;
import org.geekhub.lesson11.task2.resourses.Human;
import org.geekhub.lesson11.task2.resourses.HumanFirst;
import org.geekhub.lesson11.task2.resourses.HumanSecond;
import org.geekhub.lesson11.task2.resourses.StudentFirst;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

public class ChangeSetExtractorImplTest {

    private ChangeSetExtractorImpl extractor;
    @BeforeMethod
    public void setUp(){
        extractor = new ChangeSetExtractorImpl();
    }

    @Test
    public void shouldNotExtractChangesIfObjectsEquals(){
        String a = "String";
        String b = "String";

        Map<String, ArrayList>expectedMap = new HashMap<>();
        ChangeSet resultChangeSet = extractor.extract(a,b);

        Assert.assertEquals(resultChangeSet.changes, expectedMap);
    }

    @Test
    public void shouldNotExtractChangesIfObjectsEquals2(){
       int a = 1;
       int b = 1;

        Map<String, ArrayList>expectedMap = new HashMap<>();
        ChangeSet resultChangeSet = extractor.extract(a,b);

        Assert.assertEquals(resultChangeSet.changes, expectedMap);
    }

    @Test
    public void shouldExtractChangesIfObjectsNotEquals(){
        int a = 1;
        int b = 2;

        Map<String, Object>expectedMap = new HashMap<>();
        expectedMap.put("Old object:" , Arrays.asList(a, b));
        ChangeSet resultChangeSet = extractor.extract(a,b);

        Assert.assertEquals(resultChangeSet.changes, expectedMap);
    }

    @Test
    public void shouldExtractChangesIfObjectsAreComplicated() throws CanNotCloneException {
        CloneCreatorImpl cloneCreator = new CloneCreatorImpl();
        HumanFirst human = new HumanFirst();
        HumanFirst.Profession profession = new HumanFirst.Profession();
        profession.setType("Taxi Driver");
        human.setProfession(profession);
        human.setAge(45);
        human.setSalary("230$");

        HumanFirst clonedHuman = cloneCreator.clone(human);
        clonedHuman.getProfession().setType("Driver");
        Map<String, ArrayList<Object>> expectedMap = new HashMap<>();
        ArrayList<Object> expectedList  =  new ArrayList<>();
        expectedList.add(human.getProfession().getType());
        expectedList.add(clonedHuman.getProfession().getType());
        expectedMap.put("type", expectedList);
        ChangeSet resultChangeSet = extractor.extract(human,
                clonedHuman);

        Assert.assertEquals(resultChangeSet.changes, expectedMap);
    }

    @Test
    public void shouldNotExtractChangesIfFieldsWithDifferencesAreIgnored()  {
        HumanSecond human = new HumanSecond();
        HumanSecond.Profession profession = new HumanSecond.Profession();
        profession.setType("Florist");
        human.setProfession(profession);
        human.setAge(30);
        human.setSalary("400$");
        ///////////////////////
        HumanSecond humanSecond = new HumanSecond();
        HumanSecond.Profession secondProfession = new HumanSecond.Profession();
        secondProfession.setType("Vet");
        humanSecond.setProfession(secondProfession);
        humanSecond.setAge(30);
        humanSecond.setSalary("800$");

        ChangeSet resultChangeSet= extractor.extract(human, humanSecond);
        Map<String, ArrayList<Object>> expectedMap = new HashMap<>();


        Assert.assertEquals(resultChangeSet.changes, expectedMap);
    }

    @Test
    public void changeSetShouldProvideInfoAboutOldAndNewObjectValues() throws CanNotCloneException {
        ObjectsEquals equals = new ObjectsEquals();
        CloneCreatorImpl cloneCreator = new CloneCreatorImpl();
        HumanFirst human = new HumanFirst();
        HumanFirst.Profession profession = new HumanFirst.Profession();
        profession.setType("Taxi Driver");
        human.setProfession(profession);
        human.setAge(45);
        human.setSalary("230$");

        HumanFirst clonedHuman = cloneCreator.clone(human);
        clonedHuman.getProfession().setType("Driver");
        ChangeSet changeSet = extractor.extract(human, clonedHuman);

        Assert.assertTrue(equals.equalsComplicatedObjects(human, changeSet.getOldObject()));
        Assert.assertTrue(equals.equalsComplicatedObjects(clonedHuman, changeSet.getNewObject()));
    }

    @Test
    public void shouldNotExtractChangesIfFieldsWereNotSet()  {
        StudentFirst student = new StudentFirst();
        student.setAge(18);
        //////////////////
        StudentFirst otherStudent = new StudentFirst();
        StudentFirst.Professor professor = new StudentFirst.Professor();
        professor.setName("Alex");
        otherStudent.setProfessor(professor);
        otherStudent.setAge(18);

        ChangeSet resultChangeSet= extractor.extract(student, otherStudent);
        Map<String, ArrayList<Object>> expectedMap = new HashMap<>();


        Assert.assertEquals(resultChangeSet.changes, expectedMap);
    }

    @Test
    public void shouldExtractChangesIfObjectsAreNotEqual() throws CanNotCloneException {
        CloneCreatorImpl cloneCreator = new CloneCreatorImpl();
        Human human = new Human();
        Human.Profession.Directors directors = new Human.Profession.Directors();
        directors.setDirector("Director");
        directors.setManager("Manager");
        Human.Profession profession = new Human.Profession();
        profession.setSalary(820);
        profession.setType("Engineer");
        profession.setDirectors(directors);
        human.setProfession(profession);
        List<Human.Relatives>relativesList = new ArrayList<>();
        Human.Relatives relatives = new Human.Relatives();
        relatives.setMom("Daria");
        relatives.setDad("Andrew");
        relativesList.add(relatives);
        human.setRelativesList(relativesList);

        Human humanClone = cloneCreator.clone(human);
        humanClone.getRelativesList().get(0).setMom("Mommy");
        human.getProfession().getDirectors().setManager("Manager2");
        ArrayList<Object>arrayList1 = new ArrayList<>();
        ArrayList<Object>arrayList2 = new ArrayList<>();
        arrayList1.add(human.getProfession().getDirectors().getManager());
        arrayList1.add(humanClone.getProfession().getDirectors().getManager());
        arrayList2.add(human.getRelativesList());
        arrayList2.add(humanClone.getRelativesList());
        ChangeSet resultChangeSet= extractor.extract(human, humanClone);

        Assert.assertEquals(resultChangeSet.changes.get("Manager"), arrayList1);
        Assert.assertEquals(resultChangeSet.changes.get("relativesList"), arrayList2);
    }













}
