package org.geekhub.lesson11.task2.resourses;

import org.geekhub.lesson11.task1.CanBeCloned;

public class StudentFirst implements CanBeCloned {
    private int age;

    private Professor professor;

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Professor getProfessor() {
        return professor;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static class Professor implements CanBeCloned{
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
