package org.geekhub.lesson11;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ObjectsEquals {
    private List<Boolean> bool = new ArrayList<>();

    public boolean equalsComplicatedObjects(Object obj1, Object obj2) {
        Class A = obj1.getClass();
        if (A == obj2.getClass()) {
            if (objectBuiltInJdk(obj1)) {
                if (obj1 instanceof ArrayList && obj2 instanceof ArrayList) {
                    ArrayList a = (ArrayList) obj1;
                    ArrayList b = (ArrayList) obj2;
                    for (int i = 0; i < a.size(); i++) {
                        equalsComplicatedObjects(a.get(i), b.get(i));
                    }
                }else {
                    if (!obj1.equals(obj2)&&!Objects.equals(null, obj1)) {
                        bool.add(false);
                    }
                }
            } else {
                Field[] fields = A.getDeclaredFields();
                for (Field field : fields) {
                    if (!field.isAnnotationPresent(Ignore.class)) {
                        field.setAccessible(true);
                        try {
                           Object a = field.get(obj1);
                           Object b = field.get(obj2);
                            if (!Objects.equals(a, null) && !Objects.equals(b, null)) {
                                equalsComplicatedObjects(a, b);
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return bool.size() == 0;
    }


    public boolean objectBuiltInJdk(Object object) {
        String objectCanonicalName = object.getClass().getCanonicalName();
        if (object.getClass().isPrimitive()) {
            return true;
        } else return objectCanonicalName.startsWith("java") ||
                objectCanonicalName.startsWith("oracle") ||
                objectCanonicalName.startsWith("org.xml") ||
                objectCanonicalName.startsWith("com.oracle") ||
                objectCanonicalName.startsWith("sun") ||
                objectCanonicalName.startsWith("com.sun");
    }

    public boolean fieldBuiltInJdk(Field field) {
        String fieldCanonicalName = field.getType().getCanonicalName();
        if (field.getType().isPrimitive()) {
            return true;
        } else return fieldCanonicalName.startsWith("java") ||
                fieldCanonicalName.startsWith("oracle") ||
                fieldCanonicalName.startsWith("org.xml") ||
                fieldCanonicalName.startsWith("com.oracle") ||
                fieldCanonicalName.startsWith("sun") ||
                fieldCanonicalName.startsWith("com.sun");
    }
}
