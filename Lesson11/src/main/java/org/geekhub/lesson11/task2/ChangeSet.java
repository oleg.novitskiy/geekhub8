package org.geekhub.lesson11.task2;

import java.util.ArrayList;
import java.util.Map;

public class ChangeSet {
    public Map<String, ArrayList> changes;
    private Object oldObject;
    private Object newObject;


    public ChangeSet(Map<String, ArrayList> x) {
        this.changes = x;
    }


    public Object getNewObject() {
        return newObject;
    }

    public void setNewObject(Object newObject) {
        this.newObject = newObject;
    }

    public Object getOldObject() {
        return oldObject;
    }

    public void setOldObject(Object oldObject) {
        this.oldObject = oldObject;
    }

    @Override
    public String toString() {
        return "" + changes;
    }
}
