package org.geekhub.lesson7.task1;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

public class License {
    String type;
    private LocalDate startDate;
    private LocalDate expirationDate;
    long daysBetween;

    public License(String type, LocalDate startDate, LocalDate expirationDate) {
        this.type = type;
        this.startDate = startDate;
        this.expirationDate = expirationDate;
        this.daysBetween = DAYS.between(LocalDate.now(), expirationDate);
    }





    public long getDaysBetween() {

        return daysBetween;
    }


    public LocalDate getExpirationDate() {

        return expirationDate;

    }

    public LocalDate getStartDate() {

        return startDate;

    }

    @Override
    public String toString() {

        return "Type: " + type + " Expiration date:  " + expirationDate + " Days left: " + daysBetween;
    }


}
