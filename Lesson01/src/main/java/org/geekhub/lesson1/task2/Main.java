package org.geekhub.lesson1.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Input factorial!");
        Scanner h = new Scanner(System.in);
        int n = h.nextInt();
        FactorialCalculator.calculateFactorialOf(n);
    }
}
