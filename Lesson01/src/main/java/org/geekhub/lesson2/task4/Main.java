package org.geekhub.lesson2.task4;

public class Main extends PolynomialSolver{
    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);
        PolynomialSolver solver = new PolynomialSolver();
        System.out.println(solver.findRoots(a,b,c));
    }
}