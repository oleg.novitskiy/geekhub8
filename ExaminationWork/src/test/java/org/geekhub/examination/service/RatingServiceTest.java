package org.geekhub.examination.service;

import org.geekhub.examination.repository.RatingRepository;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.geekhub.examination.entity.Rating;
import org.geekhub.examination.mapper.ObjectMapperToDto;
import org.geekhub.examination.service.rating.service.RatingService;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class RatingServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private RatingService ratingService;
    @MockBean
    private ObjectMapperToDto objectMapperToDto;
    @MockBean
    private RatingRepository ratingRepository;

    @Test
    public void shouldUpdateRatingWhenEvaluationAlreadyExistByUser() {
        Rating rating = new Rating();
        Map<Long, Integer> uniqueUserValues = new HashMap<>();
        uniqueUserValues.put((long) 1, 5);
        rating.setUniqueUsersValues(uniqueUserValues);
        Map<Long, Integer> currentUserEvaluation = new HashMap<>();
        currentUserEvaluation.put((long)1, 10);

        rating.getUniqueUsersValues().remove((long)1);
        rating.setUniqueUsersValues(currentUserEvaluation);
        ratingService.save(rating);

        Mockito.verify(ratingRepository,Mockito.atLeastOnce()).save(rating);
    }

    @Test
    public void shouldDeleteRatingFromRatingRepository() {
        Rating rating = new Rating();

        ratingService.delete(rating);

        Mockito.verify(ratingRepository,Mockito.atLeastOnce()).delete(rating);
    }



}
