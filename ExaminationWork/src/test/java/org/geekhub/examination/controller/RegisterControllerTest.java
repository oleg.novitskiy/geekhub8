package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.geekhub.examination.config.security.config.JwtTokenProvider;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.service.user.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.nio.charset.Charset;
import java.util.Optional;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class RegisterControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private JwtTokenProvider jwtTokenProvider;
    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @BeforeMethod
    public void setUp() {

        this.mockMvc = MockMvcBuilders.standaloneSetup(new RegisterController(userService, jwtTokenProvider)).build();
    }

    @Test
    public void checkIfUserWouldBeSavedIfUsernameDoesNotExist() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setUsername("test");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(testUser);

        Mockito.when(userService.findUserByUsername("test")).thenReturn(Optional.empty());
        Mockito.when(userService.save(testUser)).thenReturn(testUser);

        mockMvc.perform(post("/register")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
         .andExpect(status().isCreated());
    }


    @Test
    public void shouldReturnStatusForbiddenIfUserAlreadyExists() throws Exception {
        AppUser testUser = new AppUser();
        testUser.setUsername("test");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(testUser);

        Mockito.when(userService.findUserByUsername("test")).thenReturn(Optional.of(testUser));
        Mockito.when(userService.save(testUser)).thenReturn(testUser);

        mockMvc.perform(post("/register")
                .contentType(APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isForbidden());
    }
}

