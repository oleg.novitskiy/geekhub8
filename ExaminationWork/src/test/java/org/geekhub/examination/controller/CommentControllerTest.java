package org.geekhub.examination.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.geekhub.examination.dto.CommentDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.entity.Comment;
import org.geekhub.examination.entity.Doctor;
import org.geekhub.examination.service.comment.service.CommentService;
import org.geekhub.examination.service.user.service.UserService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class CommentControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;

    @MockBean
    private CommentService commentService;

    private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new CommentController(userService, commentService)).build();
    }

    @Test
    public void shouldReturnJsonListOfAllCommentsFoundByUserId() throws Exception {
        CommentDto comment1 = new CommentDto();
        comment1.setContent("hello");
        CommentDto comment2 = new CommentDto();
        comment2.setContent("hi");
        List<CommentDto> comments = List.of(comment1, comment2);
        AppUser testUser = new AppUser();
        Doctor testDoctor = new Doctor();
        testDoctor.setId((long) 1);
        testUser.setDoctor(testDoctor);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String commentListInJson = ow.writeValueAsString(comments);

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.of(testUser));
        Mockito.when(commentService.getAllCommentsByDoctor((long) 1)).thenReturn(comments);

        mockMvc.perform(MockMvcRequestBuilders.get("/comment/getAll/" + 1))
                .andExpect(status().isOk())
                .andExpect(content().json(commentListInJson, true));
    }

    @Test
    public void shouldSaveCommentWhenDoctorFoundByUserId() throws Exception {
        Comment comment = new Comment();
        comment.setContent("Hello!");

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        AppUser testUser = new AppUser();
        testUser.setId((long) 1);
        Doctor testDoctor = new Doctor();
        testDoctor.setId((long) 1);
        testUser.setDoctor(testDoctor);

        AppUser testUser2 = new AppUser();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String commentInJson = ow.writeValueAsString(comment);

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.of(testUser));
        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.of(testUser2));
        Mockito.when(commentService.save(comment, testUser2, testDoctor)).thenReturn(comment);

        mockMvc.perform(MockMvcRequestBuilders.post("/comment/save/" + 1)
                .requestAttr("claims", claims)
                .contentType(APPLICATION_JSON_UTF8)
                .content(commentInJson))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldNotSaveCommentWhenDoctorNotFoundByUserId() throws Exception {
        Comment comment = new Comment();
        comment.setContent("Hello!");

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String commentInJson = ow.writeValueAsString(comment);

        Mockito.when(userService.findUserById((long) 1)).thenReturn(Optional.empty());
        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.post("/comment/save/" + 1)
                .requestAttr("claims", claims)
                .contentType(APPLICATION_JSON_UTF8)
                .content(commentInJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldDeleteCommentWhenCommentFoundByIdAndUserHasRights() throws Exception {
        Comment comment = new Comment();
        comment.setContent("Hello!");
        comment.setSenderName("andrew");

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        AppUser testUser = new AppUser();
        List<String>roles = List.of("ROLE_ADMIN");
        testUser.setRoles(roles);
        testUser.setUsername("admin");

        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.of(testUser));
        Mockito.when(commentService.findById((long) 1)).thenReturn(Optional.of(comment));

        mockMvc.perform(MockMvcRequestBuilders.post("/comment/delete/" + 1)
                .requestAttr("claims", claims))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldDeleteCommentWhenCommentFoundByIdAndUserIsAnAuthorOfTheComment() throws Exception {
        Comment comment = new Comment();
        comment.setContent("Hello!");
        comment.setSenderName("andrew");

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        AppUser testUser = new AppUser();
        List<String>roles = List.of("ROLE_USER");
        testUser.setRoles(roles);
        testUser.setUsername("andrew");

        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.of(testUser));
        Mockito.when(commentService.findById((long) 1)).thenReturn(Optional.of(comment));

        mockMvc.perform(MockMvcRequestBuilders.post("/comment/delete/" + 1)
                .requestAttr("claims", claims))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnHttpStatusForbiddenWhenUserHasNoRightsToDeleteComment() throws Exception {
        Comment comment = new Comment();
        comment.setContent("Hello!");
        comment.setSenderName("andrew");

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        AppUser testUser = new AppUser();
        List<String>roles = List.of("ROLE_USER");
        testUser.setRoles(roles);
        testUser.setUsername("someUsername");

        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.of(testUser));
        Mockito.when(commentService.findById((long) 1)).thenReturn(Optional.of(comment));

        mockMvc.perform(MockMvcRequestBuilders.post("/comment/delete/" + 1)
                .requestAttr("claims", claims))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldReturnHttpStatusNotFountWhenUserOrCommentNotFound() throws Exception {
        Comment comment = new Comment();
        comment.setContent("Hello!");
        comment.setSenderName("andrew");

        String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaWF0IjoxNTU1MDk4NzAwfQ.9OyRe3vDtr2AyYellNqbh8MPbM-uFMjrAczdM23Z4wc";
        Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();

        Mockito.when(userService.findUserByUsername("admin")).thenReturn(Optional.empty());
        Mockito.when(commentService.findById((long) 1)).thenReturn(Optional.of(comment));

        mockMvc.perform(MockMvcRequestBuilders.post("/comment/delete/" + 1)
                .requestAttr("claims", claims))
                .andExpect(status().isNotFound());
    }
}


