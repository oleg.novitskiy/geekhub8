package org.geekhub.examination.dto;

public class CommentDto {

    private Long id;

    private String content;

    private String time;

    private String senderName;

    private String imgSrc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLocalDateTime() {
        return time;
    }

    public void setTime(String localDateTime) {
        time = localDateTime;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @Override
    public String toString() {
        return time+ " " + content+ " "+ senderName;
    }
}
