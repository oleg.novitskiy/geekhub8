package org.geekhub.examination.controller;

import org.geekhub.examination.config.security.config.JwtTokenProvider;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RegisterController {

    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;

    public RegisterController(UserService userService, JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping(value = "/register")
    public ResponseEntity<AppUser> createUser(@RequestBody AppUser appUser, HttpServletResponse response)  {
        if (userService.findUserByUsername(appUser.getUsername()).isPresent()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        appUser.setRoles(roles);
        userService.save(appUser);
        jwtTokenProvider.createCurrentUserCookie(appUser, false, response);
        return new ResponseEntity<>(userService.save(appUser), HttpStatus.CREATED);

    }
}
