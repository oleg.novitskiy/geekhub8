package org.geekhub.examination.controller;

import org.geekhub.examination.dto.AppUserDto;
import org.geekhub.examination.entity.AppUser;
import org.geekhub.examination.service.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.geekhub.examination.exception.DefaultUserChangeException;
import org.geekhub.examination.exception.UserAlreadyExistsException;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {

        this.userService = userService;
    }

    @GetMapping(value = "/getNoDoctors")
    public List<AppUserDto> getAllUsersThatAreNotDoctors() {
        return userService.findAllUsersThatAreNotDoctors();
    }


    @GetMapping(value = "/get/{id}")
    public ResponseEntity userById(@PathVariable Long id) {
        if (userService.findUserById(id).isPresent()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        AppUser appUser;
        if (userService.findUserById(id).isPresent()) {
            appUser = userService.findUserById(id).get();
            if (userService.deleteUser(appUser)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @PostMapping(value = "/create")
    public ResponseEntity<AppUser> createUser(@RequestBody AppUser appUser) {
        if (userService.findUserByUsername(appUser.getUsername()).isPresent()) {
            throw new RuntimeException("Username already exists!");
        }
        return new ResponseEntity<>(userService.save(appUser), HttpStatus.CREATED);
    }


    @PutMapping(value = "/update")
    public AppUser updateUser(@RequestBody AppUser appUser) {
        if (userService.findUserByUsername(appUser.getUsername()).isPresent() &&
                !userService.findUserByUsername(appUser.getUsername())
                        .get().getId().equals(appUser.getId())) {
            throw new UserAlreadyExistsException("Unable to update.Username already exists!");
        } else if (appUser.getId().equals((long) 1)) {
            throw new DefaultUserChangeException("Unable to update default user!");
        }
        return userService.save(appUser);
    }


}
