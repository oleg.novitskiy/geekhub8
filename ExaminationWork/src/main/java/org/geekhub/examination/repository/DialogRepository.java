package org.geekhub.examination.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.geekhub.examination.entity.Dialog;

@Repository
public interface DialogRepository extends JpaRepository<Dialog, Long> {



}
