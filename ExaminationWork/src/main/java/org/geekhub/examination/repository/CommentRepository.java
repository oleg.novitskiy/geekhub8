package org.geekhub.examination.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.geekhub.examination.entity.Comment;


@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
