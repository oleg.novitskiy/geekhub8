package org.geekhub.examination.service.chat.message.service;

import org.geekhub.examination.entity.ChatMessage;

public interface MessageService {

  void save(ChatMessage chatMessage);

}
