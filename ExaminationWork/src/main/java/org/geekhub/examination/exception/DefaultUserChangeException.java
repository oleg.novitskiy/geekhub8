package org.geekhub.examination.exception;

public class DefaultUserChangeException extends RuntimeException {
    public DefaultUserChangeException(String s){
        super(s);
    }
}
