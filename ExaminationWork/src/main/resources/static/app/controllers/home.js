angular.module('ExaminationWork')
    .controller('HomeController', function ($http, $scope, $localStorage, $state, $rootScope, $cookies) {

            $http({
                method: 'GET',
                url: 'getUser',
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }

            }).success(function (res) {
                $scope.user = res;

                if (res.doctor) {

                    refreshComments(res.id);
                    $cookies.put('currentRating', res.doctor.rating.averageValue);
                }
            }).error(function () {
                $state.go('login');
            });

        function refreshComments(id) {

            $http({
                method: 'GET',
                url: 'comment/getAll/' + id,
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function (res) {
                $scope.comments = res;


            }).error(function () {
                    $state.go('login');
                }
            );
        }

        $scope.deleteAccount = function(id){

               $http.delete('user/delete/' + id, {
                   withCredentials: true,
                   headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
               }).success(function () {
                   $cookies.remove('token');
                   $rootScope.$broadcast('LogoutSuccessful');
                   $state.go('login');
               });
            };

        $scope.sendComplain = function(){
            var complaint = document.getElementById("complaint").value;
            var chatMessage = {
                sender: $rootScope.username,
                receiver: 'admin' ,
                content: complaint
            };
            $http({
                method: 'GET',
                url: 'findDialog/' + 1,
                headers: {
                    'Authorization': 'Bearer ' + $cookies.get('token')
                }
            }).success(function (res) {
                $cookies.put('dialog', res.id);
                $http.post('saveMessage/'+ res.id, chatMessage,
                    {
                        withCredentials: true,
                        headers: {'Authorization': 'Bearer ' + $cookies.get('token')}
                    }
                ).success(function (res) {

                }).error(function (error) {

                });


            }).error(function () {
                    $state.go('dialogs');
                }
            );
             $scope.message = "Successfully sent! Wait for an answer..."


        };

            $scope.rating = 0;
            $scope.ratings = [{
                current: $cookies.get('currentRating'),
                max: 10
            }];


        }
    );
