package org.geekhub.lesson18.task1;

import org.geekhub.lesson18.task1.service.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import java.util.ArrayList;
import java.util.List;

public class DAO {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CustomRowMapper customRowMapper;

    @Autowired
    public DAO(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate,
               CustomRowMapper customRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.customRowMapper = customRowMapper;
    }

    private final static String SQL_INSERT = "INSERT INTO Users (login, firstName, lastName, password, isAdmin) VALUES (?, ?, ?, ?, ?);";
    private final static String SQL_DELETE = "DELETE FROM Users WHERE Login =?";
    private final static String SQL_UPDATE = " UPDATE Users SET firstName = ?, lastName = ?, password = ?, isAdmin = ? WHERE login =?";
    private final static String SQL_AllUSERS = "SELECT * FROM USERS ORDER by firstName, login";
    private final static String SQL_FIND ="SELECT id, login, firstName, lastName, password, isAdmin FROM USERS WHERE login = :login";
    private final static String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS USERS " +
            "(id int auto_increment, " +
            " login VARCHAR(255), " +
            " firstName VARCHAR(255), " +
            " lastName VARCHAR(255), " +
            " password VARCHAR(128), " +
            " isAdmin boolean, " +
            " PRIMARY KEY ( id ))";


    public User findBy(String login) {
        return namedParameterJdbcTemplate.queryForObject(SQL_FIND,
                new MapSqlParameterSource("login", login),
                customRowMapper);
    }


    public void save(User user) {
        Object[] values = {
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.isAdmin()
        };
        List<Object[]> list = new ArrayList<>();
        list.add(values);

        jdbcTemplate.batchUpdate(SQL_INSERT, list);
    }

    public void update(User user) {
        Object[] args = new Object[]{
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.isAdmin(),
                user.getLogin()
        };
        jdbcTemplate.update(SQL_UPDATE, args);
    }

    public void delete(User user) {
        Object[] values = {
                user.getLogin()
        };

        jdbcTemplate.update(SQL_DELETE, values);
    }

    public List<User> getAllUsers() {
        return  namedParameterJdbcTemplate.query(SQL_AllUSERS, customRowMapper);
    }

    public void createTableForDatabase() {
        jdbcTemplate.execute(SQL_CREATE_TABLE);
    }
}
