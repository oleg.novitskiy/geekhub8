package org.geekhub.lesson18.task1.controllers;

import org.geekhub.lesson18.task1.EncryptPassword;
import org.geekhub.lesson18.task1.service.User;
import org.geekhub.lesson18.task1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {

    private final UserService userService;
    private final EncryptPassword encryptor;

    @Autowired
    public UserController(UserService userService, @Qualifier("passwordEncryptor") EncryptPassword encryptor) {
        this.userService = userService;
        this.encryptor = encryptor;
    }

    @GetMapping(value = {"/"})
    public String showAddPersonPage(Model model) {
        User user = new User();
        model.addAttribute("personForm", user);
        return "log_in_page";
    }


    @PostMapping(value = {"/check"})
    public String savePerson(@ModelAttribute("personForm") User user,
                             HttpSession session, Model model) {
        User userToFind = null;
        String login = user.getLogin();

        if (userService.find(login).isPresent()) {
            userToFind = userService.find(login).get();
        }
        assert userToFind != null;
        if (userToFind.getPassword().equals(encryptor.encrypt(user.getPassword()))) {
            session.setAttribute("user", userToFind);
            if(userToFind.isAdmin()){
                return "admin_hello_page";
            }else{
                model.addAttribute("users", userService.getAllUsers());
                return "simple_user_page";
            }
        }
        return "log_in_page";
    }

    @GetMapping(value = {"/home"})
    public String personList(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("userToChange", new User());
        return "home_page";
    }

    @GetMapping(value = {"/login"})
    public String showpageForLoginPrint(Model model) {
        model.addAttribute("userToFind", new User());
        return "print_login_page";
    }

    @PostMapping(value = {"/login"})
    public String findUserByLogin(@ModelAttribute("userToFind") User user, Model model) {
        String login = String.valueOf(user.getLogin());
        if(userService.find(login).isPresent()){
            model.addAttribute("users", userService.find(login).get());
            model.addAttribute("userToChange", new User());
            return "home_page";
        }
        return "print_login_page";
    }

    @GetMapping(value = {"/update"})
    public String showUpdatePage(@ModelAttribute("userToChange") User user,
                                 HttpSession session) {
      session.setAttribute("login", user.getLogin());

      return "update_page";
    }


    @PostMapping(value = {"/update"})
    public String updateUser(@ModelAttribute("userToChange") User user, @SessionAttribute String login) {
        User userToUpdate;
        if(userService.find(login).isPresent()){
            userToUpdate = userService.find(login).get();
            user.setId(userToUpdate.getId());
            user.setLogin(userToUpdate.getLogin());
            userService.update(user);
            return "redirect:/home";
        }
       return "home_page";
    }

    @PostMapping(value = {"/delete"})
    public String deleteUser(@ModelAttribute("userToChange") User user) {
        userService.delete(user);
        return "redirect:/home";
    }

    @GetMapping(value = {"/add"})
    public String showAddUserPage(Model model) {
        model.addAttribute("userToAdd", new User());
        return "add_user";
    }

    @PostMapping(value = {"/add"})
    public String addUser(@ModelAttribute("userToAdd") User user) {
        userService.add(user);
        return "redirect:/home";
    }
}
