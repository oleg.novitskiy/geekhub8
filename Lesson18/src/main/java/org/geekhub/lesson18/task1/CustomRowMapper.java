package org.geekhub.lesson18.task1;

import org.geekhub.lesson18.task1.service.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {

        User user = new User();
        user.setAdmin(rs.getBoolean("isAdmin"));
        user.setFirstName(rs.getString("firstName"));
        user.setLogin(rs.getString("login"));
        user.setId(rs.getInt("id"));
        user.setLastName(rs.getString("lastName"));
        user.setPassword(rs.getString("password"));
        return user;
    }
}
