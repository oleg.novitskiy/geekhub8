package org.geekhub.lesson13.task1;

class BrowserUnsupportedException extends RuntimeException{
    BrowserUnsupportedException(String s){
        super(s);
    }
}
//
