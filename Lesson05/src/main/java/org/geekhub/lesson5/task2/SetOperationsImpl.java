package org.geekhub.lesson5.task2;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SetOperationsImpl<Object> implements SetOperations<Object> {


    @Override
    public boolean equals(Object set1, Object set2) {

        return set1.equals(set2);
    }

    @Override
    public Set<Object> union(Object set1, Object set2) {
        Set<Object> result = new HashSet<>((Collection<? extends Object>) set1);
        result.addAll((Collection<? extends Object>) set2);


        return result;
    }

    @Override
    public Set<Object> subtract(Object set1, Object set2) {
        Set<Object> result = new HashSet<>((Collection<? extends Object>) set1);
        result.removeAll((Collection<?>) set2);
        return result;
    }

    @Override
    public Set<Object> intersect(Object set1, Object set2) {
        Set<Object> result = new HashSet<>((Collection<? extends Object>) set1);
        result.retainAll((Collection<?>) set2);
        return result;
    }

    @Override
    public Set<Object> symmetricSubtract(Object set1, Object set2) {

        Object tmpA;
        Object tmpB;
        Set<Object> tmpC;
        tmpA = (Object) subtract(set1, set2);
        tmpB = (Object) subtract(set2, set1);
        tmpC = (union(tmpA, tmpB));
        return tmpC;
    }
}
