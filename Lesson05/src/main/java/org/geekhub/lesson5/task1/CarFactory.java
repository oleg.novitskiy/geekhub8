package org.geekhub.lesson5.task1;

import java.awt.*;


public class CarFactory {

    public static Car create(String name, int power, String model, Color color) {
        return new Car(name, power, model, color);
    }


}
