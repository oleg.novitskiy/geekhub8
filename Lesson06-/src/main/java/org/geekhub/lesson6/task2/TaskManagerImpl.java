package org.geekhub.lesson6.task2;
//
import java.time.LocalDate;
import java.util.*;

public class TaskManagerImpl implements TaskManager {
    int[] taskDailyLimit;
    private List<Task> tasks;

    public TaskManagerImpl(int... dailyLimit) {
        tasks = new ArrayList<>();
        this.taskDailyLimit = dailyLimit;
        for (int a : taskDailyLimit) {
            if (a < 0) {
                throw new IllegalArgumentException("Daily limit for tasks can not be negative!");
            }
        }
    }

    @Override
    public int addTask(Task task) {
        if (taskDailyLimit == null || taskDailyLimit.length == 0) {
            tasks.add(task);
            return 1;
        } else {
            if (tasks.size() < taskDailyLimit.length) {
                tasks.add(task);
                return 1;
            } else {
                throw new TaskLimitExceededException("Amount of tasks are limited for today!");
            }
        }

    }

    @Override
    public boolean removeTasks(LocalDate yesterday) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getDate().isEqual(yesterday)) {
                tasks.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<>();
        for (Task task : tasks) {
            categories.add(task.getCategory());
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> resultMap = new HashMap<>();
        Set<String> categories = new HashSet<>();
        for (Task task : tasks) {
            categories.add(task.getCategory());
        }
        List<String> taskCategoriesList = new ArrayList<>(categories);
        for (String aTaskCategoriesList : taskCategoriesList) {
            resultMap.put(aTaskCategoriesList, getTasksByCategory(aTaskCategoriesList));
        }
        return resultMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        ArrayList<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getCategory().equals(category)) {
                taskList.add(task);
            }
        }
        Collections.sort(taskList);

        return taskList;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> tasksForToday = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getDate().isEqual(LocalDate.now())) {
                tasksForToday.add(task);
            }
        }
        tasksForToday.sort(new compareByTaskCategoryName());

        return tasksForToday;
    }

    @Override
    public List<Task> getAllTasks() {
        return new ArrayList<>(tasks);
    }

    public static class compareByTaskCategoryName implements Comparator<Task> {
        @Override
        public int compare(Task o1, Task o2) {
            return o1.getCategory().compareTo(o2.getCategory());
        }

    }

}



