package org.geekhub.lesson6.task1;


public class Car implements Comparable<Car> {
    private int power;
    private String name;
    private Color color;

    public Car(String name, int power, Color color) {
        this.power = power;
        this.name = name;
        this.color = color;
    }

    @Override
    public boolean equals(Object obj) {
        if(this.getClass().equals(obj.getClass()) ){
            Car carr = (Car) obj;
            return  this.color.equals(carr.color)&&
                    this.power==carr.power&&
                    this.name.equals(carr.name);
        }
        return false;
    }


    @Override
    public int compareTo(Car o) {
        double lumThis = (0.2126*this.color.getI()) + (0.7152*this.color.getI1())+
                (0.0722*this.color.getI2());
        double lum = (0.2126*o.color.getI()) + (0.7152*o.color.getI1())+
                (0.0722*o.color.getI2());
        if (this.name == null || o.name == null) {
            return 0;
        }
        int result = this.name.compareTo(o.name);
        if (result == 0) {
            result = o.power - this.power;
           if(result ==0){
               result = (int) (lumThis - lum);
           }
        }
        return result;
    }
}

