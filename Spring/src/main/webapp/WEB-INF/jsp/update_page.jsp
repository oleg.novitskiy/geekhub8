<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update</title>
</head>
<body>
<h3>Update user: </h3>
<table border="0" cellspacing="0" cellpadding="10" width="10%" height="10">
    <form action="/users/update" method="post">
        <input type="hidden" name="action" value="UPDATE">
        <input type="hidden" name="login" value=${pageContext.session.getAttribute('login')}>
        <tr>First name: <input type="text" name="firstName"></tr>
        <br><br>
        <tr>Last name*: <input type="text" name="lastName"></tr>
        <br><br>
        <tr>Password: <input type=password name="password"></tr>
        <br><br>
        <tr><input type="radio" name="isAdmin" value="true">ADMIN</tr>
        <br><br>
        <tr><input type="radio" name="isAdmin" value="false">NOT ADMIN</tr>
        <br><br>
        <tr><input type="submit" value="Update"></tr>
        <br><br>
    </form>
</table>
</body>
</html>
