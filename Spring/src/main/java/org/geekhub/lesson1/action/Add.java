package org.geekhub.lesson1.action;
import org.geekhub.lesson1.dao.User;
import org.geekhub.lesson1.dao.UserService;


public class Add implements Action{

    @Override
    public void apply(UserService source, User user) {
        source.add(user);
    }


}
