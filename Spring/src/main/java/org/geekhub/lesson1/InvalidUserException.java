package org.geekhub.lesson1;

public class InvalidUserException extends RuntimeException {
    public InvalidUserException(String s){
        super(s);
    }

}
