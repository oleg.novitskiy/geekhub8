package org.geekhub.lesson1;

import org.springframework.stereotype.Component;


import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Iterator;
import java.util.logging.Logger;

@Component
public class RequestLoggingInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = Logger.getLogger(RequestLoggingInterceptor.class.getName());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        long startTime = Instant.now().toEpochMilli();
        Iterator iter = request.getParameterNames().asIterator();
        while (iter.hasNext()) {
            String parameterName = String.valueOf(iter.next());
            logger.info("PARAMETER ATTRIBUTE: " + parameterName);
        }
        logger.info("Request URL:" + request.getRequestURL().toString() +
                "  Start Time=" + Instant.now() + " METHOD: " + request.getMethod());
        request.setAttribute("startTime", startTime);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws IOException {
        long startTime = (Long) request.getAttribute("startTime");
        if (ex != null) {

            logger.info("Request URL::" + request.getRequestURL().toString() +
                    ":: Time Taken=" + (Instant.now().toEpochMilli() - startTime) +
                    "REQUEST METHOD: " + request.getMethod() + " EXCEPTION: " +
                    ex.getMessage());
            response.sendRedirect("error.html");
        } else {

            logger.info("Request URL::" + request.getRequestURL().toString() +//
                    ":: Time Taken=" + (Instant.now().toEpochMilli() - startTime) +
                    "REQUEST METHOD: " + request.getMethod());

        }

    }
}




