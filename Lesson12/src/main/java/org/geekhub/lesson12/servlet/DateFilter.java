package org.geekhub.lesson12.servlet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.time.LocalDate;


@WebFilter(urlPatterns = {"/*"})
public class DateFilter implements Filter {

    @Override
    public void init(FilterConfig cfg) {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {

        String dayOfWeek = LocalDate.now().getDayOfWeek().name();

        if ( dayOfWeek.equals("SATURDAY")||dayOfWeek.equals("SUNDAY")) {
          throw new RuntimeException("Access denied! Please, attend this site weekdays");

        }

        chain.doFilter(req, resp);


    }

    @Override
    public void destroy() {

    }


}
