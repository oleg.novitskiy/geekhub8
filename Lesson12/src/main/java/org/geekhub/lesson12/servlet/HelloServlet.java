package org.geekhub.lesson12.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "helloServlet", urlPatterns = {"/hello"})

public class HelloServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.getWriter().print("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "    <title>HelloServlet</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<form action=\"session\" && action=\"username\" &&action=\"value\" method=\"POST\">\n" +
                "    <p>Action:</p>\n" +
                "    <select name=\"action\">\n" +
                "        <option label value=\"1\" selected></option>\n" +
                "        <option value=\"Add\">Add</option>\n" +
                "        <option value=\"Update\">Update</option>\n" +
                "        <option value=\"Invalidate\">Invalidate</option>\n" +
                "        <option value=\"Remove\">Remove</option>\n" +
                "    </select>\n" +
                "\n" +
                "    <p>Name:</p>\n" +
                "    <input type=\"text\" name=\"username\">\n" +
                "    <p>Value: </p>\n" +
                "    <input type=\"text\" name=\"value\">\n" +
                "    <br>\n" +
                "    <br>\n" +
                "    <input type=\"submit\" value=\"Submit\"/>\n" +
                "\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>");
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
        HttpSession session = request.getSession();
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();


        String username = request.getParameter("username");
        String value = request.getParameter("value");
        String action = request.getParameter("action");

        if (action.equals("Add")) {
            if(username.isEmpty()||value.isEmpty()) {
                throw new RuntimeException("Please, input parameters!");
            }else{
                session.setAttribute(username, value);
            }
        }
        if (action.equals("Remove")) {
            if(username.isEmpty()) {
                throw  new IllegalArgumentException("Please, input parameters!");
            }else{
                session.removeAttribute(username);
            }
        }
        if (action.equals("Update")) {
            if(username.isEmpty()||value.isEmpty()) {
                throw  new IllegalArgumentException("Please, input parameters!");
            }else{
                session.removeAttribute(username);
                session.setAttribute(username, value);
            }
        }
        if (action.equals("Invalidate")) {
            session.invalidate();
        }

        writer.println("<!DOCTYPE HTML>");
        writer.println("<html><body><p>" + "name: " + username + "<br>" + "value:" + value + " " + " </p></body></html>");
    }
}
