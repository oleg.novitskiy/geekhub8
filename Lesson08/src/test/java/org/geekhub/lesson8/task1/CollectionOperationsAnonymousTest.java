package org.geekhub.lesson8.task1;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.testng.Assert.*;

public class CollectionOperationsAnonymousTest {
    private CollectionOperations collectionOperations;


    @BeforeMethod
    public void setUp() {
        collectionOperations = new CollectionOperationsImpl();
    }


    @Test
    public void shouldCreateListOfObjects() {
        Supplier<User> producer = new Supplier() {
            @Override
            public Object get() {
                return new User();
            }
        };
        List<User> resultList = collectionOperations.fill(producer, 3);

        Assert.assertEquals(resultList.size(), 3);
        Assert.assertEquals(resultList.get(0).toString(), "USER");
        Assert.assertEquals(resultList.get(1).toString(), "USER");
        Assert.assertEquals(resultList.get(2).toString(), "USER");
    }

    @Test
    public void shouldCreateEmptyListOfObjects() {
        Supplier<User> producer = new Supplier() {
            @Override
            public Object get() {
                return new User();
            }
        };
        List<User> resultList = collectionOperations.fill(producer, 0);

        Assert.assertEquals(resultList.size(), 0);

    }

    @Test
    public void shouldFilterListBySomeRule() {
        List<String> fruits = Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet");
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Orange");
            }
        };

        List<String> resultList = collectionOperations.filter(fruits, filter);
        List<String> expectedList = Collections.singletonList("Orange");

        assertCollectionEqualsIgnoringOrder(resultList, expectedList);
    }

    @Test
    public void shouldCreateEmptyListWhenFilterReturnsEmptyData() {
        List<String> fruits = Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet");
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Orrange");
            }
        };

        List<String> resultList = collectionOperations.filter(fruits, filter);
        List<String> expectedList = new ArrayList<>();

        assertEquals(resultList, expectedList);
    }


    @Test
    public void shouldReturnTrueIfAtLeastOneObjMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Orange");
            }
        };
        boolean result = collectionOperations.anyMatch(fruits, filter);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfNoneObjMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple",
                "Avocado", "Banana", "Apple", "Garnet"));
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Data");
            }
        };
        boolean result = collectionOperations.anyMatch(fruits, filter);

        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnFalseIfAtLeastOneObjDoesntMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Orange");
            }
        };
        boolean result = collectionOperations.allMatch(fruits, filter);

        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfAllObjMatchTheRule() {
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Orange");
            }
        };
        List<String> testList = Arrays.asList("Orange", "Orange", "Orange");
        boolean result = collectionOperations.allMatch(testList, filter);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfAtLeastOneObjectMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Orange");
            }
        };
        boolean result = collectionOperations.noneMatch(fruits, filter);

        Assert.assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfNoneObjectMatchTheRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Computer");
            }
        };
        boolean result = collectionOperations.noneMatch(fruits, filter);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldAppSomeActionToEachObject() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        List<String> testingList = new ArrayList<>();

        Consumer<String> consumer = new Consumer() {
            @Override
            public void accept(Object o) {
                testingList.add(o.toString().concat("Fruit"));
            }
        };

        collectionOperations.forEach(fruits, consumer);

        Assert.assertEquals(testingList.size(), 7);
        Assert.assertTrue(testingList.contains("OrangeFruit"));
        Assert.assertTrue(testingList.contains("GarnetFruit"));

    }

    @Test
    public void shouldTransformAllObjInListFromIntegerToString() {
        Function<Integer, String> Function = new Function() {
            @Override
            public Object apply(Object o) {
                return o.toString();
            }
        };

        List<Integer> testList = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<String> resultList = collectionOperations.map(testList, Function);
        List<String> expectedList = Arrays.asList("1", "2", "3", "4", "5", "6");

        assertCollectionEqualsIgnoringOrder(resultList, expectedList);
    }

    @Test
    public void shouldReturnEmptyListForEmptyInputToMap() {
        Function<Integer, String> Function = new Function() {
            @Override
            public Object apply(Object o) {
                return o.toString();
            }
        };

        List<Integer> testList = new ArrayList<>();
        List<String> resultList = collectionOperations.map(testList, Function);
        List<String> expectedList = new ArrayList<>();

        assertEquals(resultList, expectedList);
    }


    @Test
    public void shouldFindMaxElementInList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Comparator<String> comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return o1.toString().compareTo(o2.toString());
            }
        };

        Optional<String> result = collectionOperations.max(fruits, comparator);
        Optional<String> expectedResult = Optional.of("Apple");

        Assert.assertEquals(result, expectedResult);

    }

    @Test
    public void shouldReturnOptionalEmptyForEmptyMaxInput() {
        Comparator<String> comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return o1.toString().compareTo(o2.toString());
            }
        };
        List<String> fruitsTest = new ArrayList<>();

        Optional<String> result1 = collectionOperations.max(fruitsTest, comparator);

        Assert.assertTrue(result1.isEmpty());
    }

    @Test
    public void shouldFindMinElementInList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Comparator<String> comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return o1.toString().compareTo(o2.toString());
            }
        };

        Optional<String> result = collectionOperations.min(fruits, comparator);
        Optional<String> expectedResult = Optional.of("Orange");

        Assert.assertEquals(result, expectedResult);

    }

    @Test
    public void shouldReturnOptionalEmptyForEmptyMinInput() {
        Comparator<String> comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return o1.toString().compareTo(o2.toString());
            }
        };
        List<String> fruitsTest = new ArrayList<>();

        Optional<String> result1 = collectionOperations.min(fruitsTest, comparator);

        Assert.assertTrue(result1.isEmpty());
    }

    @Test
    public void shouldReturnUniqueObjFromList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        List<String> result = collectionOperations.distinct(fruits);
        List<String> expectedResult = Arrays.asList("Garnet", "Apple", "Avocado", "Orange", "Lemon", "Banana");

        assertCollectionEqualsIgnoringOrder(result, expectedResult);
    }

    @Test
    public void shouldReturnEmptyListIfNoInputPresent() {
        List<String> testList = new ArrayList<>();
        List<String> result = collectionOperations.distinct(testList);
        List<String> expectedResult = new ArrayList<>();

        assertCollectionEqualsIgnoringOrder(result, expectedResult);
    }

    @Test
    public void shouldReduceAllObjectsFromList() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        BinaryOperator<String> binaryOperator = new BinaryOperator() {
            @Override
            public Object apply(Object o, Object o2) {
                return o.toString().concat(o2.toString());
            }
        };
        Optional<String> result = collectionOperations.reduce(fruits, binaryOperator);
        Optional<String> expectedResult = Optional.of("OrangeLemonAppleAvocadoBananaAppleGarnet");

        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void shouldReduceObjectsWithSpecifiedObject() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        BinaryOperator<String> binaryOperator = new BinaryOperator() {
            @Override
            public Object apply(Object o, Object o2) {
                return o.toString().concat(o2.toString());
            }
        };
        String result = collectionOperations.reduce("Summer", fruits, binaryOperator);
        String expectedResult = "OrangeLemonAppleAvocadoBananaAppleGarnetSummer";
        Assert.assertEquals(result, expectedResult);

    }

    @Test
    public void shouldReturnSpecifiedObjWhenListOfObjectsIsEmpty() {
        List<String> emptyList = new ArrayList<>();
        BinaryOperator<String> binaryOperator = new BinaryOperator() {
            @Override
            public Object apply(Object o, Object o2) {
                return o.toString().concat(o2.toString());
            }
        };
        String result = collectionOperations.reduce("Summer", emptyList, binaryOperator);

        Assert.assertEquals(result, "Summer");

    }

    @Test
    public void objectShouldBePartitionedBySomeRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Apple");
            }
        };
        List<String> listFalse = Arrays.asList("Lemon", "Avocado", "Banana", "Garnet", "Orange");
        List<String> listTrue = Arrays.asList("Apple", "Apple");
        Map<Boolean, List<String>> result = collectionOperations.partitionBy(fruits, filter);

        assertCollectionEqualsIgnoringOrder(result.get(true), listTrue);
        assertCollectionEqualsIgnoringOrder(result.get(false), listFalse);

    }

    @Test
    public void objectShouldBePartitionedBySomeRuleTestForEmptyInput() {
        Predicate<String> filter = new Predicate() {
            @Override
            public boolean test(Object o) {
                return o.toString().equals("Apple");
            }
        };
        List<String> emptyList = new ArrayList<>();

        Map<Boolean, List<String>> resultMap = collectionOperations.partitionBy(emptyList, filter);

        assertCollectionEqualsIgnoringOrder(resultMap.get(true), emptyList);
        assertCollectionEqualsIgnoringOrder(resultMap.get(false), emptyList);
    }

    @Test
    public void objectShouldBeGroupedInMapBySomeRule() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));

        Function<String, String> functionForMapping = new Function() {
            @Override
            public Object apply(Object o) {
                String x = "Fruits";
                String y = "Vegetables";
                if (o.toString().equals("Orange") || o.toString().equals("Apple") || o.toString().equals("Lemon") ||
                        o.toString().equals("Banana")) {
                    return x;
                } else {
                    return y;
                }

            }
        };

        Map<String, List<String>> result = collectionOperations.groupBy(fruits, functionForMapping);
        List<String> listWithFruits = Arrays.asList("Orange", "Lemon", "Apple", "Banana", "Apple");
        List<String> listWithVegetables = Arrays.asList("Avocado", "Garnet");


        assertCollectionEqualsIgnoringOrder(result.get("Fruits"), listWithFruits);
        assertCollectionEqualsIgnoringOrder(result.get("Vegetables"), listWithVegetables);

    }

    @Test
    public void shouldAccumulateObjectsIntoMap() {
        List<String> fruits = new ArrayList<>(Arrays.asList("Orange", "Lemon", "Apple", "Avocado",
                "Banana", "Apple", "Garnet"));

        BinaryOperator<String> binaryOperator = new BinaryOperator() {
            @Override
            public Object apply(Object o, Object o2) {
                return o.toString().concat(o2.toString());
            }
        };
        Function<String, String> valueFunction = new Function() {
            @Override
            public Object apply(Object o) {
                return String.valueOf(o).toUpperCase();
            }
        };
        Function<String, String> keyFunction = new Function() {
            @Override
            public Object apply(Object o) {
                String x = "Green";
                String y = "Yellow";
                String z = "Orange";
                String c = "Red";
                switch (o.toString()) {
                    case "Banana":
                    case "Lemon":
                        return y;
                    case "Apple":
                    case "Avocado":
                        return x;
                    case "Garnet":
                        return c;
                    default:
                        return z;
                }
            }
        };

        Map<String, String> result = collectionOperations.toMap(fruits, keyFunction, valueFunction, binaryOperator);

        Map<String, String> expectedResult = new HashMap<>();
        expectedResult.put("Red", "GARNET");
        expectedResult.put("Green", "APPLEAVOCADOAPPLE");
        expectedResult.put("Orange", "ORANGE");
        expectedResult.put("Yellow", "LEMONBANANA");


        Assert.assertEquals(result, expectedResult);

    }

    private <T> void assertCollectionEqualsIgnoringOrder(Collection<T> expected, Collection<T> actual) {
        if (actual == expected) {
            return;
        }
        assertNotNull(actual);
        assertNotNull(expected);

        assertEquals(actual.size(), expected.size(), "Collections are different in size.");
        for (T item : expected) {
            assertTrue(actual.contains(item), "Result collection does not contain expected value.");
        }
    }


}



