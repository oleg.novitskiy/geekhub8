package org.geekhub.lesson8.task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class Lambdas {


    public static void main(String[] args) {
        CollectionOperationsImpl collectionOperations = new CollectionOperationsImpl();
        List<String> fruits = new ArrayList<>();
        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Lemon");
        fruits.add("Apple");


        Supplier<User> producer = () -> new User();
        Predicate<String> filter = string -> string.equals("Apple");
        Consumer<String> consumer = x -> x.concat("Fruit");
        Function<String, String> function = x -> x.toUpperCase();
        Comparator<String> comparator = (x1, x2) -> {
            return x1.compareTo(x2);
        };
        BinaryOperator<String> binaryOperator = (x1, x2) -> {
            return x1.concat(x2);
        };
        Function function1 = x -> {
            Object o = "Fruits";
            Object o1 = "Vegetables";
            if (x.toString().equals("Orange") || x.toString().equals("Apple") || x.toString().equals("Lemon") ||
                    x.toString().equals("Banana")) {
                return o;
            } else {
                return o1;

            }

        };
        Function keyfunction = x -> {
            Object n = "Green";
            Object y = "Yellow";
            Object z = "Orange";
            Object c = "Red";
            if (x.toString().equals("Banana") || x.toString().equals("Lemon")) {
                return y;
            } else if (x.toString().equals("Apple") || x.toString().equals("Avocado")) {
                return n;
            } else if (x.toString().equals("Garnet")) {
                return c;
            } else {
                return z;
            }


        };


        System.out.println(collectionOperations.fill(producer, 5));
        System.out.println(collectionOperations.filter(fruits, filter));
        System.out.println(collectionOperations.anyMatch(fruits, filter));
        System.out.println(collectionOperations.allMatch(fruits, filter));
        collectionOperations.forEach(fruits, consumer);
        System.out.println(collectionOperations.noneMatch(fruits, filter));
        System.out.println(collectionOperations.map(fruits, function));
        System.out.println(collectionOperations.max(fruits, comparator));
        System.out.println(collectionOperations.min(fruits, comparator));
        System.out.println(collectionOperations.distinct(fruits));
        System.out.println(collectionOperations.reduce(fruits, binaryOperator));
        System.out.println(collectionOperations.partitionBy(fruits, filter));
        System.out.println(collectionOperations.groupBy(fruits, function1));
        collectionOperations.toMap(fruits, keyfunction, function, binaryOperator);


    }
}
