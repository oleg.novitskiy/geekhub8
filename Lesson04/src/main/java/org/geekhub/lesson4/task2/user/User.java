package org.geekhub.lesson4.task2.user;

public class User {
    private int i;
    public String username;
    public String password;
    public User(int i, String username, String password) {
        this.i = i;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return i;
    }
}
