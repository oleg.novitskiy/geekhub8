package org.geekhub.lesson10.task2;

import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {

        FolderManagement folderManagement = new FolderManagement();
        final String currentPath = System.getProperty("user.dir");

        System.out.println("Choose action, print: 'create' - create folder," +
                "'rename' - rename folder" + "'delete' -  delete folder");

        String codeOfAction = args[0];

        System.out.println("Input folder name:");
        String folderName = args[1];


        if (codeOfAction.equals("create")) {
            if (folderManagement.checkIfFolderDoesNotExist(folderName, currentPath)) {
                System.out.println(folderManagement.addFolder(folderName, currentPath));
            } else {
                System.out.println("Folder already exists. Press 1 to replace:");
                String s = args[2];
                if (s.equals("1")) {
                    System.out.println(folderManagement.deleteFolder(folderName, currentPath));
                    System.out.println(folderManagement.addFolder(folderName, currentPath));
                } else {
                    System.out.println("Good bye");
                }
            }
        }
        if (codeOfAction.equals("rename")) {
            if (folderManagement.checkIfFolderDoesNotExist(folderName, currentPath)) {
                System.out.println("Folder does not exist!");
            } else {
                System.out.println("Please, type new name of folder:");
                String newName = args[3];
                if (folderManagement.checkIfFolderDoesNotExist(folderName, newName)) {
                    System.out.println(folderManagement.renameFolder(Paths.get
                            (currentPath + "\\" + folderName), newName));
                }
            }
        }
        if (codeOfAction.equals("delete")) {
            if (folderManagement.checkIfFolderDoesNotExist(folderName, currentPath)) {
                System.out.println("Folder not found!");
            } else {
                System.out.println(
                        folderManagement.deleteFolder(folderName, currentPath));
            }
        }
    }
}
