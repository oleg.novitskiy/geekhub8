package org.techforumist.jwt.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.Claims;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.techforumist.jwt.CreateAndFullDatabaseOnStart;
import org.techforumist.jwt.domain.AppUser;
import org.techforumist.jwt.service.user_service.UserService;

@RestController
public class RegisterLoginController {

	private static final int Twenty_Eight_Days_In_Seconds = 2419200;
	private static final int Two_Hours_In_Seconds = 7200;


	private final CreateAndFullDatabaseOnStart createAndFullDatabaseOnStart;
	private final UserService userService;


	@Autowired
	public RegisterLoginController(CreateAndFullDatabaseOnStart createAndFullDatabaseOnStart, UserService userService) {
		this.createAndFullDatabaseOnStart = createAndFullDatabaseOnStart;
		this.userService = userService;

	}


	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<AppUser> createUser(@RequestBody AppUser appUser, HttpServletResponse response) {

		if (userService.findUserByUsername(appUser.getUsername()) != null) {
			throw new RuntimeException("Email already exist");
		}
		List<String> roles = new ArrayList<>();
		roles.add("USER");
		appUser.setRoles(roles);
		userService.save(appUser);

		response.addCookie(createCurrentUserCookie(appUser, false));

		return new ResponseEntity<>(userService.save(appUser), HttpStatus.CREATED);
	}


	@RequestMapping("/user")
	public AppUser user(Principal principal) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String loggedUsername = auth.getName();
		return userService.findUserByUsername(loggedUsername);
	}


	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> login(@RequestParam String username, @RequestParam String password,
													 HttpServletRequest request,
													 HttpServletResponse response) {


		AppUser currentUser = userService.findUserByUsername(username);

		if (currentUser != null && currentUser.getPassword().equals(password)) {
			if (request.getParameter("rememberMe")!=null) {
				response.addCookie(createCurrentUserCookie(currentUser, true));
				System.out.println("Authenticated");
			} else {
				response.addCookie(createCurrentUserCookie(currentUser, false));
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping(value = "/recover", method = RequestMethod.POST)
	public ResponseEntity recoverUserCredentials(@RequestParam String passphrase,
																	  @RequestParam String password,
																	  HttpServletResponse response
													                   ) {
		AppUser currentUser = userService.findUserByPassPhrase(passphrase);

		if(currentUser!=null){
			currentUser.setPassword(password);
			userService.save(currentUser);
			response.addCookie(createCurrentUserCookie(currentUser, false));

			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@GetMapping(value = "/getUser")
	public AppUser confirmCurrentUser(HttpServletRequest request, HttpServletResponse response) {
		Claims claims = (Claims) request.getAttribute("claims");
		AppUser currentUser = userService.findUserByUsername(claims.getSubject());
		if(claims.get("rememberMe") == null){
			response.addCookie(createCurrentUserCookie(currentUser, false));
		}else{
			response.addCookie(createCurrentUserCookie(currentUser,true));
		}
		return currentUser;
	}

	private Cookie createCurrentUserCookie(AppUser currentUser, boolean rememberMe) {
		Cookie cookie;
		if (rememberMe) {
			cookie = new Cookie("token", createCurrentUserToken(currentUser, true));
			cookie.setMaxAge(Twenty_Eight_Days_In_Seconds);
		} else {
			cookie = new Cookie("token", createCurrentUserToken(currentUser, false));
			cookie.setMaxAge(Two_Hours_In_Seconds);
		}
		return cookie;
	}

	private String createCurrentUserToken(AppUser currentUser, boolean rememberMe) {
		String token;
		if (rememberMe) {
			token = Jwts.builder().setSubject(currentUser.getUsername())
					.claim("roles", currentUser.getRoles())
					.claim("rememberMe", "rememberMe")
					.setIssuedAt(new Date())
					.signWith(SignatureAlgorithm.HS256, "secretkey")
					.compact();
		} else {
			token = Jwts.builder().setSubject(currentUser.getUsername())
					.claim("roles", currentUser.getRoles())
					.setIssuedAt(new Date())
					.signWith(SignatureAlgorithm.HS256, "secretkey")
					.compact();
		}
		return token;
	}

	@RequestMapping(value = "/createDatabase", method = RequestMethod.GET)
	public String createDatabase() {
		createAndFullDatabaseOnStart.onApplicationEvent();
		return "databaseCreated";
	}

}

