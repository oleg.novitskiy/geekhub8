package org.techforumist.jwt.config.security_config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configurable
@EnableWebSecurity

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


	@Override
	public void configure(WebSecurity web) {

		web.ignoring()

				.antMatchers("/", "/index.html", "/app/**", "/register","/recover","/createDatabase", "/authenticate","/favicon.ico"
						, "/chat", "/info", "/ws/**",  "/createDoctor"
				);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http


				.authorizeRequests()


				.antMatchers("/info").permitAll()


				.anyRequest().fullyAuthenticated()

				.and()

				.addFilterBefore(new WebTokenFilter(), UsernamePasswordAuthenticationFilter.class)

				.httpBasic().and()

				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

				.csrf().disable();
	}

}
