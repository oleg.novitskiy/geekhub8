package org.geekhub.lesson19.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.geekhub.lesson19.persistence.User;
import org.geekhub.lesson19.service.user.service.UserService;
import org.geekhub.lesson19.service.session.service.SessionService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;
import java.util.Optional;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestExecutionListeners(MockitoTestExecutionListener.class)
public class UserControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private SessionService sessionService;


    @BeforeMethod
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new UserController(userService, sessionService)).build();
    }

    @Test
    public void shouldReturnJsonListOfAllUsers() throws Exception {
        User user1 = new User();
        User user2 = new User();
        user1.setUsername("user1");
        user2.setUsername("user2");
        List<User>allUsers = List.of(user1, user2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String allUsersJson = ow.writeValueAsString(allUsers);

        Mockito.when(userService.getAllUsers())
                .thenReturn(allUsers);


        mockMvc.perform(MockMvcRequestBuilders.get("/user/getAll"))
                .andExpect(status().isOk())
                .andExpect(content().json(allUsersJson, true));
    }

    @Test
    public void shouldFindUserByUsernameIfExists() throws Exception {
        User user1 = new User();
        user1.setUsername("user1");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String userInJson = ow.writeValueAsString(List.of(user1));

        Mockito.when(userService.find("user1"))
                .thenReturn(Optional.of(user1));

        mockMvc.perform(MockMvcRequestBuilders.get("/user/get/user1"))
                .andExpect(status().isOk())
                .andExpect(content().json(userInJson, true));
    }








}


