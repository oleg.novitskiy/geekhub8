 angular.module('Lesson19').controller('LoginController', function($http, $scope, $rootScope, $state) {

    $scope.authenticate = function(){
        $http({
            method: 'GET',
            url: 'login',
            params: {
                username: $scope.login ,
                password: $scope.password
            }
        }).success(function (res) {

                  if(res.admin ===true){
                      $state.go('users');
                  }else{
                      $state.go('simple');
                  }


        });
    };

});


