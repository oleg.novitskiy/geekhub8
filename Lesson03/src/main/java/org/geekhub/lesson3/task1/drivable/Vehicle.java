package org.geekhub.lesson3.task1.drivable;

import org.geekhub.lesson3.task1.drivable.constituents.*;

public abstract class Vehicle extends Drivable {


    int a;
    double b;
    Engine eng = new Engine(b, a);
    GasTank tank = new GasTank(a);
    Accelerator accel = new Accelerator(a);
    String carName = new String();
    int maxNumberOfPassengers;
    int numberOfDoors;
    SteeringWheel steeringWhee1ll = new SteeringWheel(a, a);

    public Vehicle(String vehicle, int i, int i1, Accelerator accelerator, BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel) {
        this.steeringWhee1ll = steeringWheel;
        this.eng = engine;
        this.tank = gasTank;
        this.accel = accelerator;
        this.carName = vehicle;
        this.maxNumberOfPassengers = i;
        this.numberOfDoors = i1;


    }

    public Vehicle() {
    }


    public int speed() {
        return 0;
    }

    public void turnLeft() {
        steeringWhee1ll.turnLeft();
    }

    public void turnRight() {
        steeringWhee1ll.turnRight();
    }

    public void stopTheEngine() {
        eng.stop();
    }

    public void startTheEngine() {
        eng.start();
    }

    public void fillTank(double i) {
        tank.fill(i);
    }

    public void accelerate() {
        accel.accelerate(eng, tank);

    }

    public void brake() {
    }

    public String getName() {
        return carName;

    }

    public int getMaxNumberOfPassengers() {

        return maxNumberOfPassengers;
    }

    public int getNumberOfDoors() {

        return numberOfDoors;
    }


}
