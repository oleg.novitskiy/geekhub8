package org.geekhub.lesson3.task1.controlpanel;

import org.geekhub.lesson3.task1.drivable.Drivable;
import org.geekhub.lesson3.task1.drivable.Drivable;
import org.geekhub.lesson3.task1.drivable.cars.Hatchback3Doors;
import org.geekhub.lesson3.task1.drivable.cars.Hatchback5Doors;
import org.geekhub.lesson3.task1.drivable.cars.SUV;
import org.geekhub.lesson3.task1.drivable.cars.Sedan;
import org.geekhub.lesson3.task1.drivable.constituents.Accelerator;
import org.geekhub.lesson3.task1.drivable.constituents.BrakePedal;
import org.geekhub.lesson3.task1.drivable.constituents.Engine;
import org.geekhub.lesson3.task1.drivable.constituents.GasTank;
import org.geekhub.lesson3.task1.drivable.constituents.SteeringWheel;
import org.geekhub.lesson3.task1.drivable.constituents.*;

public class ControlPanelImpl implements ControlPanel {

    int a;
    double b;
    SteeringWheel wheel = new SteeringWheel(a, a);
    GasTank tank = new GasTank(a);
    Drivable car = new Drivable() {
        @Override
        public void startTheEngine() {

        }

        @Override
        public int speed() {
            return 0;
        }

        @Override
        public void accelerate() {

        }
    };

    public ControlPanelImpl() {
        this.wheel = wheel;
        this.car = car;
        this.tank = tank;

    }


    public void turnLeft(Drivable car) {
        wheel.turnLeft();
    }


    public void turnRight(Drivable car) {
        wheel.turnRight();
    }

    @Override
    public void accelerate(Drivable car) {
        car.accelerate();
    }

    @Override
    public int brake(Drivable car) {

       return 0;
    }

    @Override
    public void fillTank(Drivable car, int i) {
        tank.fill(i);

    }


}
