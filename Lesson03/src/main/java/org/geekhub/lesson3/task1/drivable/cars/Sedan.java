package org.geekhub.lesson3.task1.drivable.cars;

import org.geekhub.lesson3.task1.drivable.Vehicle;
import org.geekhub.lesson3.task1.drivable.constituents.*;

public class Sedan extends Vehicle {
    int numberOfPassengers = 4;
    int numberOfDoors = 5;
    String carName;

    public Sedan(String lancer_x, Accelerator accelerator, BrakePedal brakePedal, Engine engine, GasTank gasTank, SteeringWheel steeringWheel) {
        this.carName = lancer_x;

    }



    public String getName() {
        return carName;
    }


    public int getMaxNumberOfPassengers() {
        return numberOfDoors;
    }


    public int getNumberOfDoors() {
        return numberOfPassengers;
    }

}
