package org.geekhub.lesson3.task1.drivable;

public abstract class Drivable {


    public abstract void startTheEngine();

    public abstract int speed();

    public abstract void accelerate();
}
